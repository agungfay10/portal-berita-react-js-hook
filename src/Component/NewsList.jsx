import { useEffect, useState } from "react";
import Axios from "axios";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import "bootstrap/dist/css/bootstrap.min.css";
import { MagnifyingGlass } from "react-loader-spinner";

const NewsList = () => {
  const [articles, setArticles] = useState([]);
  const [searchFilter, setSearchFilter] = useState([]);
  const [result, setResult] = useState("");
  const [isLoading, setLoading] = useState(true)

  useEffect(() => {
    const getArticles = async () => {
      try {
        setLoading(true);
        const res = await Axios.get(
          "https://newsapi.org/v2/everything?domains=liputan6.com,tribunnews.com,detik.com&apiKey=f592cc399956459d8420183485fc80a9"
          // "https://newsapi.org/v2/top-headlines?country=id&category=business&apiKey=f592cc399956459d8420183485fc80a9"
        );
        setArticles(res.data.articles);
        setSearchFilter(res.data.articles);
        setLoading(false);
        //   setArticles(res.data.articles);
        //   console.log(res);
      } catch(error) {
        setLoading(false);
        console.error(error);
      }
    };
    getArticles();
  }, []);

  useEffect(() => {
    const results = searchFilter.filter((res) =>
      res.title.toLowerCase().includes(result)
    );
    setArticles(results);
  }, [result, searchFilter]);

  const onChange = (evt) => {
    setResult(evt.target.value);
  };

  return (
    <div>
      <Form className="d-flex mt-4">
        <Form.Control
          type="search"
          placeholder="Search"
          className="me-2"
          value={result}
          onChange={onChange}
          aria-label="Search"
        />
        <Button variant="outline-success">Search</Button>
      </Form>
      <div className="d-flex justify-content-around row mt-3">
        {!isLoading ? (
            articles.map(
              ({ title, url, author, description, urlToImage, publishedAt }) => {
                var tgl = publishedAt.split("T");
                return (
                  <div
                    className="col-3 card mt-3 p-2"
                    style={{ width: "22rem" }}
                    key={title}
                  >
                    <img src={urlToImage} className="card-img-top" alt="" />
                    <div>
                      <h5>{title}</h5>
                      <h6 className="text-secondary">
                        {author} - {tgl[0]} {tgl[1]}
                      </h6>
                      <p>{description}</p>
                      <Button href={url} variant="success">
                        Details
                      </Button>
                    </div>
                  </div>
                );
              }
            )
        ):(
            <MagnifyingGlass
              visible={true}
              height="80"
              width="80"
              ariaLabel="MagnifyingGlass-loading"
              wrapperStyle={{}}
              wrapperClass="MagnifyingGlass-wrapper"
              glassColor="#c0efff"
              color="#198754"
            />
          )
        }
      </div>
    </div>
  );
};

export default NewsList;
